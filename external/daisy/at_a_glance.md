

# 1 DAISY at a Glance

DAISY is a tool that assists GDPR compliance by keeping a register of personal data used in research. DAISY's application menu Bar lists the main functions provided.

 ![Alt](../../external/img/menubar.png "DAISY Menubar")
 <center>DAISY Menubar</center>

 * _Project Management_ module allows for the recording of research activities as projects. Documenting projects is critical for GDPR compliance as projects constitute the purpose and the context of use of personal data. Any document supporting the legal and ethical basis for data use can be stored in DAISY. Examples are ethics approvals, consent configurations, or subject information sheets.
 * _Dataset Management_ module allows for the recording of personal data held by the institution. The dataset may or may not fall in the context of a particular project. DAISY allows   Datasets to be defined in a granular way; where - if desired - each data subset, called a data declaration, can be listed individually. These declarations may list data from a particular partner, data of a particular cohort or data of a particular type.
 * _Contract Management_ module allows the recording and storage of legal contracts of various types that have been signed with partner institutes or suppliers. Consortium agreements, data sharing agreements, material transfer agreements are examples of contracts. For GDPR compliance contracts become useful when documenting the source of  datasets received or the target datasets transferred.
 * _Definitions Management_. DAISY comes pre-packed with default lookup lists; these can be changed during application deployment. Lookup lists are used when defining contracts, projects or datasets. The definitions module of the DAISY application allows the management of dynamic lookup lists, specifically those of cohorts, partner institutes and contact persons.

The dependencies between DAISY modules is given below. There are no hard dependencies between the Projects, Contracts and Datasets modules. In principle you may start using any of these modules once DAISY is deployed with pre-packed definitions.

![Alt](../../external/img/dependencies.png "DAISY module dependencies")
 <center>DAISY module dependencies</center>

<mark>Our suggestion to first-time users is the following</mark>:

1. Familiarise yourself with DAISY's  interface conventions by reading the [relevant section of this guide](#DIC).
2. Login to DAISY with a user that has Administrator or VIP privileges. E.g. In the demo deployment the _admin_ user has Administrator privileges and _alice.white_ has VIP privileges.
3. Use Project Management to [create a project](#PM1).
4. Use Dataset Management to [create a dataset](#PM21) within the defined project.



<br />
<br />
<br />
<div style="text-align: right;"> <a href="#top">Back to top</a> </div>
<br />
<br />
<br />
