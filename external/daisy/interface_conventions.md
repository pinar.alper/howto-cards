
<a name="DIC"></a>
# 2 DAISY Interface Conventions

<a name="SP"></a>
## 2.1 Search Pages


DAISY provides search pages for all entities manageable via modules. Currently these are: _Projects, Contracts, Datasets_, and, under definitions, _Partners, Cohorts, Contacts_.  All search pages have similar layout and operational conventions. Search pages are also the only entry point for the functions in a module. When you select a module from the menu bar, you will be taken to the search page for the entity managed by that module.

A screenshot of the search page for Projects is given below.
Each search page is headed with _help text search facets on the left hand side and search results on the right hand side.


 ![Alt](../../external/img/search_page.png)
 <center>Search Page for Projects</center>

By default, all entities - in our example, all projects- will be listed on the search page. The list can be filtered by either selecting one or more facet from the left hand side or by typing in a keyword into the search box. Currently DAISY search  does not support partial matching; instead the entire keyword will be matched in a case insensitive  manner.

On the top right section of search results a few attributes will be listed. Clicking on these attributes repeatedly will respectively (1) enable the ordering either (2) change order to ascending/descending (3) disable ordering for the clicked attribute.

Each entity listed in the search results will be displayed in a shaded box are listing few of attributes; in our example the project's name and number of publications are listed. Each result box will also contain a  "DETAILS" link, through which  you can go to the [Entity Details Page](#EDP).

Depending on the permissions associated with your user type, you may see a **add button (denoted with a plus sign)** at the bottom right section of the search page. You can add a new entity by clicking  the plus button, which will open up an empty editor form for you to fill in.


<a name="EDP"></a>
## 2.2 Entity Details Pages

Information about a single entity is provided through a Details Page. As example page for a _Project_ named ``SYSCID'' is given below.

![Alt](../../external/img/details_page.png)
<center>Details page of a Project in DAISY</center>

You may end up on an Entity Details Page in two primary ways.  (1) Through the  ``DETAILS'' link of a search results in a search page  (2) Through links on details pages of  other (linked) entities in DAISY.

Each Details Page is headed with an **overview box) listing some of  the entity's attributes. Depending on the permissions associated with your user type, you may see an **edit entity button (denoted with a pencil icon)** and an **permissions button (denoted with an eye icon)**. These will take you to the Entity Editor Page and the  Permissions Management Page respectively.

Beneath the overview box several information boxes will be listed displaying further details of the entity.

If you have edit permissions for the entity, then at the top right corner of some detail boxes you will see an **add detail button (denoted with a plus sign)**. Via this button you can do the following:

* create links to other entities e.g.  link contacts with projects.
* create (inline) detail records to the current entity e.g. one or more publications to a project.

<a name="EEP"></a>
## 2.3 Entity Editor Pages


When you click the edit button on the Details Page of an entity, you will be taken to the Editor Page containing a form for entity update.  An example is given below.

![Alt](../../external/img/editor_form.png)
<center>Editor page of a Project</center>

Each field in the form will be listed with a **Name**, a **Value** and a **Help text**. Names of fields that are required to have a value will end with a red asterisk.

Editor forms can be saved by pressing the **SUBMIT** button found at the bottom of the page. Forms will be validated upon submission. If validation fails for one or more fields, these will be highlighted with inline validation _ERROR_ messages, illustrated below.

![Alt](../../external/img/validation_error.png)
<center>Field validation error</center>


Upon successful save of a form, you will be returned to the entity details page. DAISY may give **SUCCESS** and/or **WARNING** messages upon form save; these will be displayed at the top of the page, as illustrated below.

 ![Alt](../../external/img/page_messages.png)
<center>Message display in DAISY</center>


<br />
<br />
<br />
<div style="text-align: right;"> <a href="#top">Back to top</a> </div>
<br />
<br />
<br />