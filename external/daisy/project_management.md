
<a name="PM"></a>
# 3 Project Management

 <a name="PM1"></a>
## 3.1 Create New Project

<mark>In order  to create a new project</mark>:

1. Click Projects from the Menu Bar.<br />
 ![Alt](../../external/img/project_menubar.png)
2. Click the add button from the Project Search Page.<br />
 ![Alt](../../external/img/add_button.png)
3. You will see an empty Project Form. _Acronym, Title_ and _Local Custodians_  are mandatory fields, whereas others are optional. Provide values for fields. Please note that one of the Local Custodians must be a user with VIP Privileges. E.g. in the demo deployment _alice.white_ is a  research principle investigator and is a VIP user.<br />
![Alt](../../external/img/project_custodians.png)
4. Click SUBMIT. Once you successfully save the form, you will be taken to the newly create project's details page, as seen below.<br />
![Alt](../../external/img/project_created.png)


<a name="PM2"></a>
## 3.2 Manage Project Details


When you first create a _Project_ in DAISY, it will be isolated, with no links to other entities. The _Project_ page provides shortcuts to create such entities, most importantly Datasets and Contracts. If you use these shortcuts the newly created entities will automatically be linked to the project.  In the following section we describe how such shortcuts work.
<a name="PM21"></a>
### 3.2.1 Add Dataset to Project


When a project is created, it will have no associated Datasets. On the project's details page this situation will be highlighted in the **Data of this project** detail box as follows:

 ![Alt](../../external/img/project_add_dataset.png)

 <mark>In order to create a Dataset and automatically link it to the project</mark>:

 1. Click the **Add** button pointed by the arrow  in the **Data of this project** detail box.
 2. You will see the **Dataset Creation Quick Form** as below. The _Project_ field will be auto-selected, in addition you need to provide _Local Custodians_ and a _Title_ for your dataset. Typically the local custodians would be the same as the project, but you are free to choose any user as the custodian. Remember that one of the custodians needs to be a VIP User. Provide the field values and press submit.\
  ![Alt](../../external/img/project_add_dataset_form.png)

  3. Once the dataset is created, you will be taken to the **Dataset's Details Page**, seen below. You can continue working on the data set as described in [this part of the guide](#DM). If you want to go pack to the Project that owns this dataset, then you can click the project link in the dataset's overview box, highlighted below.<br />
  ![Alt](../../external/img/project_add_dataset_result.png)

<a name="PM22"></a>
### 3.2.2 Add Contract to Project

TBC

<a name="PM23"></a>
### 3.2.3 Add Personnel to Project


A project's _Personnel_ refer to those persons that work on the project, we assume that these persons will all have a user account for the DAISY system.  The **Personnel** detail box on the _Project_ page also allows linking DAISY _Users_ as personnel of a Project, to do so;


1. Click the plus button  on the **Personnel** details box, as seen below.<br />
 ![Alt](../../external/img/project_ref_user.png)
2. You will be asked to select one user from a searchable list of existing users. Users will be listed with their name and surname. Make a selection and click SUBMIT. <br />
 ![Alt](../../external/img/project_ref_user_search.png)


 Personnel can be unlinked from a Project by clicking on the trash icon that will appear when hovering over items in the  **Personnel detail box**, as seen below. Note that the personnel will not be deleted from the system, it will simply be unlinked.<br />


  ![Alt](../../external/img/project_remove_user.png)

<a name="PM24"></a>
### 3.2.4 Manage Project Contacts


A project's _Contacts_ refer to those persons that are associated with the Project, but that are not a user of the DAISY system. Under the _Definitions Management_ module, it is possible to manage _Contacts_ and search for them. The **Contacts** detail box on the _Project_ page also allows contact management, as follows;

 1. Click the plus button  on the **Contacts** details box. You will be given the options to either **Create a new contact** or **Reference an existing contact** as seen below.<br />
 ![Alt](../../external/img/project_add_contact.png)

 2. If you choose to reference, you will be asked to select one contact from a searchable list of existing contacts. Contacts will be listed with their name, surname and role as seen below. Make a selection and click SUBMIT.<br />
 ![Alt](../../external/img/project_ref_contact.png)

 3. If you choose to create new, you will see the contact creation form. Once you fill in the details and click SUBMIT a new contact will be created and automatically linked to the project.<br />
 ![Alt](../../external/img/project_add_contact_form.png)


 Contacts can be unlinked from a Project by clicking on the trash icon that will appear when hovering over items in the  **Contacts detail box**, as seen below. Note that the contact will not be deleted from the system, it will simply be unlinked.<br />
![Alt](../../external/img/project_remove_contact.png)


<a name="PM25"></a>
### 3.2.5 Manage Project Documentation


You can attach documents to a project record in DAISY. Examples of documents are the Project Proposal, Ethics Approvals, Templates of Consent Form or the Subject Information Sheet. Documents can be in any format, PDF, Word or scans. If the document is stored already in an external system, you can also record its link in DAISY.

Document management is done via the **Documents** detail box, as follows:

1. Click the plus button on the **Documents** detail box.<br />
![Alt](../../external/img/project_add_document.png)

2. You will see the _Document_ creation form. Either select _Content_, i.e. file to upload, or paste the document link (if in external system). You will be required to provide a brief description of the document in _Document Notes_. Also you will also be required to select a _Type_ for the document. <br />
![Alt](../../external/img/project_add_document_form.png)

3. Click SUBMIT and the new document will be listed as below. Documents can be deleted by clicking on the trash icon beside each document, also document information can be edited by clicking the pencil icon.<br />
![Alt](../../external/img/project_remove_document.png)



### 3.2.6 Manage Project Publications
<a name="PM26"></a>

A project's _Publications_ can be managed via the **Publications detail box**, as follows:

 1. Click the plus button  on the **Publications** detail box. You will be given the options to either **Create a new publication** or **Reference an existing publication** as seen below.<br />
 ![Alt](../../external/img/project_add_publication.png)

 2. If you choose to reference, you will be asked to select one publication from a searchable list of existing publications. Publications will be listed with their citation string. Make a selection and click SUBMIT.<br />
 ![Alt](../../external/img/project_ref_publication.png)

 3. If you choose to create new, you will see the publication creation form asking you for the _Citation string_ and the  _DOI_ for the paper. Once you fill in the details and click SUBMIT a new publication will be created and automatically linked to the project.<br />
 ![Alt](../../external/img/project_add_publication_form.png)


 Publications can be unlinked from a Project by clicking on the trash icon that will appear when hovering over items in the **Publications detail box**, as seen below. Note that the publication will not be deleted from the system, it will simply be unlinked.<br />


![Alt](../../external/img/project_remove_publication.png)


<br />
<br />
<br />
<div style="text-align: right;"> <a href="#top">Back to top</a> </div>
<br />
<br />
<br />


