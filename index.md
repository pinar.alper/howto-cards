---
layout: default
title: Home
order: 1
---

# Research Data Management

Bioinformatics Core assists Luxembourg Centre for Systems Biomedicine ([LCSB](https://www.uni.lu/lcsb)) researchers with the organization, management, and curation of research data through its R3 initiative. This page is an index for lab cards  that are intended to provide practical guidance  in  implementing Data Management, Data Protection and IT setup.

## Current howto-cards

  * [Exchange data with collaborators](./external/exchange-channels/)
  * [Registering human-subject data to DAISY](./external/daisy/)
  * [Markdown markup language](./external/markdown/)
  * [LUMS account](./external/lums-passwords)
  * [Git clients](./external/git-clients/)
